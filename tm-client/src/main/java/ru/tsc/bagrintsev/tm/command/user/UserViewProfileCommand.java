package ru.tsc.bagrintsev.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.user.UserViewProfileRequest;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        @Nullable final User user = getAuthEndpoint().viewProfile(new UserViewProfileRequest(getToken())).getUser();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
        System.out.println("LOCKED: " + user.getLocked());
    }

    @NotNull
    @Override
    public String getDescription() {
        return "View user profile.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-view-profile";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
