package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    Integer getPasswordHashIterations();

    @NotNull
    Integer getPasswordHashKeyLength();

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

}
