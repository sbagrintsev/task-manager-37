package ru.tsc.bagrintsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.repository.ICommandRepository;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.bagrintsev.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> names = new TreeMap<>();

    @NotNull
    private final Map<String, AbstractCommand> shortNames = new TreeMap<>();

    @Override
    public void add(@NotNull final AbstractCommand command) {
        Optional.of(command.getName())
                .ifPresent(n -> {
                    if (!n.isEmpty()) names.put(n, command);
                });
        Optional.of(command.getShortName())
                .ifPresent(n -> {
                    if (!n.isEmpty()) shortNames.put(n, command);
                });
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAvailableCommands() {
        return names.values();
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) throws CommandNotSupportedException {
        @NotNull final Optional<AbstractCommand> command = Optional.ofNullable(names.get(name));
        command.orElseThrow(() -> new CommandNotSupportedException(name));
        return command.get();
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByShort(@NotNull final String shortName) throws ArgumentNotSupportedException {
        @NotNull final Optional<AbstractCommand> command = Optional.ofNullable(shortNames.get(shortName));
        command.orElseThrow(() -> new ArgumentNotSupportedException(shortName));
        return command.get();
    }

    @NotNull
    @Override
    public Iterable<AbstractCommand> getShortCommands() {
        return shortNames.values();
    }

}
