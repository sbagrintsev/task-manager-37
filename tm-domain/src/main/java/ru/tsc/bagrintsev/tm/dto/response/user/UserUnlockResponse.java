package ru.tsc.bagrintsev.tm.dto.response.user;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class UserUnlockResponse extends AbstractUserResponse {

}
