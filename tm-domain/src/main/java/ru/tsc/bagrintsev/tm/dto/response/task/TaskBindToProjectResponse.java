package ru.tsc.bagrintsev.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.model.Task;

@NoArgsConstructor
public final class TaskBindToProjectResponse extends AbstractTaskResponse {

    public TaskBindToProjectResponse(@Nullable final Task task) {
        super(task);
    }

}
