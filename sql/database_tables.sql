create table if not exists m_user
(
    id            varchar(100) not null,
    login         varchar(100) not null,
    password_hash varchar(50)  not null,
    password_salt bytea        not null,
    email         varchar(255),
    first_name    varchar(50),
    middle_name   varchar(50),
    last_name     varchar(50),
    role          varchar(20)  not null,
    is_locked     boolean      not null,
    primary key (id)
);

create table if not exists m_project
(
    id            varchar(100) not null,
    user_id       varchar(100),
    name          varchar(255) not null,
    description   varchar(255) not null,
    status        varchar(20)  not null,
    date_created  timestamptz  not null,
    date_started  timestamptz,
    date_finished timestamptz,
    last_name     varchar(50),
    primary key (id),
    foreign key (user_id) references m_user (id)
);

create table if not exists m_task
(
    id            varchar(100) not null,
    project_id    varchar(100),
    user_id       varchar(100),
    name          varchar(255) not null,
    description   varchar(255) not null,
    status        varchar(20)  not null,
    date_created  timestamptz  not null,
    date_started  timestamptz,
    date_finished timestamptz,
    last_name     varchar(50),
    primary key (id),
    foreign key (user_id) references m_user (id),
    foreign key (project_id) references m_project (id) on delete cascade
);
