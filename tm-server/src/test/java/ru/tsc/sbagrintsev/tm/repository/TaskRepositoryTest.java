package ru.tsc.sbagrintsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.IncorrectRoleException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.repository.TaskRepository;
import ru.tsc.bagrintsev.tm.service.ConnectionService;
import ru.tsc.bagrintsev.tm.service.PropertyService;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Category(DBCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private final String userId = "testUserId1";

    @NotNull
    private final String userId2 = "testUserId2";

    @NotNull
    private TaskRepository taskRepository;

    @Before
    public void setUp() {
        @NotNull final PropertyService propertyService = new PropertyService();
        @NotNull final ConnectionService connectionService = new ConnectionService(propertyService);
        taskRepository = new TaskRepository(connectionService.getConnection());
    }

    @Test
    @Category(DBCategory.class)
    public void testAdd() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull final Task Task = new Task();
        taskRepository.add(Task);
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertEquals(Task, taskRepository.findAll().get(0));
    }

    @Test
    @Category(DBCategory.class)
    public void testAddCollection() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull final List<Task> TaskList = new ArrayList<>();
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        TaskList.add(task1);
        TaskList.add(task2);
        taskRepository.add(TaskList);
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertEquals(2, taskRepository.totalCount());
    }

    @Test
    @Category(DBCategory.class)
    public void testClear() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull final Task Task = new Task();
        Task.setId("id1");
        Task.setName("name1");
        @NotNull final Task task2 = new Task();
        task2.setId("id2");
        task2.setName("name2");
        taskRepository.add(userId, Task);
        taskRepository.add(userId, task2);
        @NotNull final Task Task3 = new Task();
        Task.setId("id3");
        Task.setName("name3");
        taskRepository.add(userId2, Task3);
        Assert.assertEquals(2, taskRepository.findAll(userId).size());
        Assert.assertEquals(1, taskRepository.findAll(userId2).size());
        taskRepository.clear(userId);
        Assert.assertEquals(0, taskRepository.findAll(userId).size());
        Assert.assertEquals(1, taskRepository.findAll(userId2).size());
    }

    @Test
    @Category(DBCategory.class)
    public void testCreate() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        taskRepository.create("id1", "name1");
        taskRepository.create("id2", "name2", "description2");
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertEquals("name1", taskRepository.findAll().get(0).getName());
        Assert.assertEquals("description2", taskRepository.findAll().get(1).getDescription());
    }

    @Test(expected = ModelNotFoundException.class)
    @Category(DBCategory.class)
    public void testExistsById() throws AbstractException, SQLException {
        @NotNull final Task Task = new Task();
        Task.setId("id1");
        Task.setName("name1");
        taskRepository.add(userId, Task);
        Assert.assertTrue(taskRepository.existsById(userId, "id1"));
        Assert.assertTrue(taskRepository.existsById(userId, "id2"));
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAll() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        taskRepository.create(userId, "name1");
        taskRepository.create(userId, "name2");
        taskRepository.create(userId2, "name3");
        Assert.assertEquals(2, taskRepository.findAll(userId).size());
        Assert.assertEquals(1, taskRepository.findAll(userId2).size());
        Assert.assertEquals(3, taskRepository.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAllByProjectId() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull final Task task1 = new Task();
        task1.setProjectId("project1");
        @NotNull final Task task2 = new Task();
        task2.setProjectId("project2");
        taskRepository.add(userId, task1);
        taskRepository.add(userId, task2);
        Assert.assertEquals(2, taskRepository.findAll(userId).size());
        Assert.assertEquals(1, taskRepository.findAllByProjectId(userId, "project1").size());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAllSorted() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        taskRepository.create(userId, "name8");
        taskRepository.create(userId, "name6");
        taskRepository.create(userId2, "name3");
        taskRepository.create(userId2, "name1");
        Assert.assertEquals("name6", taskRepository.findAll(userId, Sort.BY_NAME.getComparator()).get(0).getName());
        Assert.assertEquals("name3", taskRepository.findAll(userId2, Sort.BY_CREATED.getComparator()).get(0).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindOneById() throws AbstractException, SQLException {
        @NotNull final Task Task = new Task();
        Task.setId("id1");
        Task.setName("name1");
        @NotNull final Task task2 = new Task();
        task2.setId("id2");
        task2.setName("name2");
        taskRepository.add(userId, Task);
        taskRepository.add(userId, task2);
        Assert.assertEquals("name1", taskRepository.findOneById(userId, "id1").getName());
        Assert.assertEquals("name2", taskRepository.findOneById(userId, "id2").getName());
    }

    @Test
    @Category(DBCategory.class)
    public void testRemove() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull final Task Task = new Task();
        Task.setId("id1");
        Task.setName("name1");
        taskRepository.add(userId, Task);
        Assert.assertTrue(taskRepository.findAll(userId).contains(Task));
        Assert.assertEquals(Task, taskRepository.remove(userId, Task));
        Assert.assertFalse(taskRepository.findAll(userId).contains(Task));
    }

    @Test
    @Category(DBCategory.class)
    public void testRemoveAll() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull final List<Task> TaskList = new ArrayList<>();
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        TaskList.add(task1);
        TaskList.add(task2);
        taskRepository.add(TaskList);
        Assert.assertEquals(2, taskRepository.findAll().size());
        taskRepository.removeAll(TaskList);
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test(expected = ModelNotFoundException.class)
    @Category(DBCategory.class)
    public void testRemoveById() throws AbstractException, SQLException {
        @NotNull final Task Task = new Task();
        Task.setId("id1");
        Task.setName("name1");
        taskRepository.add(userId, Task);
        Assert.assertTrue(taskRepository.findAll(userId).contains(Task));
        Assert.assertEquals(Task, taskRepository.removeById(userId, "id1"));
        Assert.assertFalse(taskRepository.findAll(userId).contains(Task));
        Assert.assertEquals(Task, taskRepository.removeById(userId, "id1"));
    }

    @Test(expected = ModelNotFoundException.class)
    @Category(DBCategory.class)
    public void testSetProjectId() throws AbstractException, SQLException {
        @NotNull final Task task1 = new Task();
        task1.setId("task1");
        taskRepository.add(userId, task1);
        Assert.assertNull(taskRepository.findAll(userId).get(0).getProjectId());
        taskRepository.setProjectId(userId, "task1", "project1");
        Assert.assertEquals("project1", taskRepository.findAll(userId).get(0).getProjectId());
        taskRepository.setProjectId(userId, "task2", "project1");
    }

    @Test
    @Category(DBCategory.class)
    public void testTotalCount() throws SQLException {
        @NotNull final Task Task = new Task();
        Task.setId("id1");
        Task.setName("name1");
        @NotNull final Task task2 = new Task();
        task2.setId("id2");
        task2.setName("name2");
        taskRepository.add(userId, Task);
        taskRepository.add(userId, task2);
        Assert.assertEquals(2, taskRepository.totalCount());
    }

}
