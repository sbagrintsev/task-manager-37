package ru.tsc.sbagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.service.*;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

import java.sql.SQLException;

@Category(DBCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private final String userId = "testUserId1";

    @NotNull
    private ProjectService projectService;

    @NotNull
    private TaskService taskService;

    @NotNull
    private ProjectTaskService projectTaskService;

    @NotNull
    private ConnectionService connectionService;

    @NotNull
    private PropertyService propertyService;

    @NotNull
    private Project project;

    @NotNull
    private Task task;

    @Before
    public void setUp() throws AbstractException {
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        taskService = new TaskService(connectionService);
        projectService = new ProjectService(connectionService);
        projectTaskService = new ProjectTaskService(projectService, taskService);
        project = new Project();
        project.setId("projectId1");
        project.setName("projectName1");
        projectService.add(userId, project);
        task = new Task();
        task.setId("taskId1");
        task.setName("taskName1");
        taskService.add(userId, task);
    }

    @Test
    @Category(DBCategory.class)
    public void testBindTaskToProject() throws AbstractException, SQLException {
        Assert.assertNull(taskService.findAll().get(0).getProjectId());
        projectTaskService.bindTaskToProject(userId, "projectId1", "taskId1");
        Assert.assertEquals("projectId1", taskService.findOneById("taskId1").getProjectId());
    }

    @Test
    @Category(DBCategory.class)
    public void testRemoveProjectById() throws AbstractException, SQLException {
        Assert.assertFalse(projectService.findAll().isEmpty());
        Assert.assertFalse(taskService.findAll().isEmpty());
        projectTaskService.bindTaskToProject(userId, "projectId1", "taskId1");
        projectTaskService.removeProjectById(userId, "projectId1");
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void testUnbindTaskFromProject() throws AbstractException, SQLException {
        projectTaskService.bindTaskToProject(userId, "projectId1", "taskId1");
        Assert.assertEquals("projectId1", taskService.findOneById("taskId1").getProjectId());
        projectTaskService.unbindTaskFromProject(userId, "taskId1");
        Assert.assertNull(taskService.findAll().get(0).getProjectId());
    }

}
