package ru.tsc.bagrintsev.tm.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.IDomainService;
import ru.tsc.bagrintsev.tm.api.sevice.IServiceLocator;
import ru.tsc.bagrintsev.tm.dto.request.data.*;
import ru.tsc.bagrintsev.tm.dto.response.data.*;
import ru.tsc.bagrintsev.tm.enumerated.Role;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.bagrintsev.tm.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IDomainService getDomainService() {
        return getServiceLocator().getDomainService();
    }

    @NotNull
    @Override
    @WebMethod
    public BackupLoadResponse loadBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final BackupLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadBackup();
        return new BackupLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64LoadResponse loadBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64LoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinaryLoadResponse loadBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinaryLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJacksonJsonLoadResponse loadJacksonJson(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonJsonLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadJacksonJSON();
        return new DataJacksonJsonLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJacksonXmlLoadResponse loadJacksonXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonXmlLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadJacksonXML();
        return new DataJacksonXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJacksonYamlLoadResponse loadJacksonYaml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonYamlLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadJacksonYAML();
        return new DataJacksonYamlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJaxbJsonLoadResponse loadJaxbJson(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJaxbJsonLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadJaxbJSON();
        return new DataJaxbJsonLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJaxbXmlLoadResponse loadJaxbXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJaxbXmlLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadJaxbXML();
        return new DataJaxbXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public BackupSaveResponse saveBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final BackupSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveBackup();
        return new BackupSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64SaveResponse saveBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64SaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinarySaveResponse saveBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinarySaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJacksonJsonSaveResponse saveJacksonJson(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonJsonSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveJacksonJSON();
        return new DataJacksonJsonSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJacksonXmlSaveResponse saveJacksonXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonXmlSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveJacksonXML();
        return new DataJacksonXmlSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJacksonYamlSaveResponse saveJacksonYaml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonYamlSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveJacksonYAML();
        return new DataJacksonYamlSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJaxbJsonSaveResponse saveJaxbJson(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJaxbJsonSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveJaxbJSON();
        return new DataJaxbJsonSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJaxbXmlSaveResponse saveJaxbXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJaxbXmlSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveJaxbXML();
        return new DataJaxbXmlSaveResponse();
    }

}
