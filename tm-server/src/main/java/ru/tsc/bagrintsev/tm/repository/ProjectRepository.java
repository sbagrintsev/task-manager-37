package ru.tsc.bagrintsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(Project.class, connection);
    }

    @NotNull
    @Override
    public Project add(@NotNull final Project project) throws SQLException {
        @NotNull final String sql = String.format("" +
                        "INSERT INTO %s (" +
                        "   id, date_created, name, description, status, user_id, date_started, date_finished) " +
                        "VALUES (" +
                        "   ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setTimestamp(2, javaDateToSql(project.getDateCreated()));
            statement.setString(3, project.getName());
            statement.setString(4, project.getDescription());
            statement.setString(5, project.getStatus().toString());
            statement.setString(6, project.getUserId());
            statement.setTimestamp(7, javaDateToSql(project.getDateStarted()));
            statement.setTimestamp(8, javaDateToSql(project.getDateFinished()));
            statement.executeUpdate();
        }
        return project;
    }

    @NotNull
    @Override
    protected Project fetch(@NotNull final ResultSet rowSet) throws SQLException, IncorrectStatusException {
        @NotNull final Project project = new Project();
        project.setId(rowSet.getString("id"));
        project.setDateCreated(rowSet.getTimestamp("date_created"));
        project.setName(rowSet.getString("name"));
        project.setDescription(rowSet.getString("description"));
        project.setStatus(Status.toStatus(rowSet.getString("status")));
        project.setUserId(rowSet.getString("user_id"));
        project.setDateStarted(rowSet.getTimestamp("date_started"));
        project.setDateFinished(rowSet.getTimestamp("date_finished"));
        return project;
    }

    @NotNull
    @Override
    protected String getTableName() {
        return "m_project";
    }

}
