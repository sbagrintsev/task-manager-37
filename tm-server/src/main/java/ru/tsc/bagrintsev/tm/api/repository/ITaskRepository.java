package ru.tsc.bagrintsev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) throws SQLException, IncorrectStatusException;

    Task setProjectId(
            @NotNull final String userId,
            @NotNull final String taskId,
            @Nullable final String projectId
    ) throws AbstractException, SQLException;

}
