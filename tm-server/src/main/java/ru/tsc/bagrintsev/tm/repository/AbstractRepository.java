package ru.tsc.bagrintsev.tm.repository;

import org.eclipse.persistence.internal.oxm.StrBuffer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.IAbstractRepository;
import ru.tsc.bagrintsev.tm.comparator.DateStartedComparator;
import ru.tsc.bagrintsev.tm.comparator.NameComparator;
import ru.tsc.bagrintsev.tm.comparator.StatusComparator;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.IncorrectRoleException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.model.AbstractModel;

import java.sql.*;
import java.util.*;
import java.util.Date;

public abstract class AbstractRepository<M extends AbstractModel> implements IAbstractRepository<M> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    @Override
    public abstract M add(@NotNull final M record) throws SQLException;

    @NotNull
    protected String getQueryOrder(@NotNull final Comparator comparator) {
        if (comparator.equals(NameComparator.INSTANCE)) return "name";
        else if (comparator.equals(StatusComparator.INSTANCE)) return "status";
        else if (comparator.equals(DateStartedComparator.INSTANCE)) return "date_started";
        else return "date_created";
    }

    @NotNull
    public Collection<M> add(@NotNull final Collection<M> records) throws SQLException {
        @NotNull List<M> result = new ArrayList<>();
        for (M record : records) {
            result.add(add(record));
        }
        return result;
    }

    @Override
    public void clear() throws SQLException {
        @NotNull final String sql = String.format("" +
                "DELETE FROM %s;", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    @Override
    public boolean existsById(@NotNull final String id) throws AbstractException, SQLException {
        findOneById(id);
        return true;
    }

    @NotNull
    protected abstract M fetch(@NotNull final ResultSet rowSet) throws SQLException, IncorrectStatusException, IncorrectRoleException;

    @NotNull
    @Override
    public List<M> findAll() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull List<M> records = new ArrayList<>();
        @NotNull final String sql = String.format("" +
                "SELECT * " +
                "FROM %s;", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet rowSet = statement.executeQuery(sql);
            while (rowSet.next()) records.add(fetch(rowSet));
        }
        return records;
    }

    @NotNull
    @Override
    public List<M> findAll(
            @NotNull final Comparator comparator
    ) throws SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull List<M> records = new ArrayList<>();
        @NotNull String order = getQueryOrder(comparator);
        @NotNull final String sql = String.format("" +
                "SELECT * " +
                "FROM %s " +
                "ORDER BY %s;", getTableName(), order);
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet rowSet = statement.executeQuery(sql);
            while (rowSet.next()) records.add(fetch(rowSet));
        }
        return records;
    }

    @NotNull
    @Override
    public M findOneById(@NotNull final String id) throws AbstractException, SQLException {
        @NotNull final String sql = String.format("" +
                "SELECT * " +
                "FROM %s " +
                "WHERE id = ?;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) throw new ModelNotFoundException();
            return fetch(rowSet);
        }
    }

    @NotNull
    protected abstract String getTableName();

    @Nullable
    protected Timestamp javaDateToSql(@Nullable final Date date) {
        if (date == null) return null;
        return new Timestamp(date.getTime());
    }

    @NotNull
    @Override
    public M remove(@NotNull final M record) throws SQLException {
        @NotNull final String sql = String.format("" +
                "DELETE * " +
                "FROM %s " +
                "WHERE id = ?;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, record.getId());
            statement.executeUpdate();
        }
        return record;
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collection) throws SQLException {
        @NotNull final String sql = String.format("" +
                "DELETE * " +
                "FROM %s " +
                "WHERE id in (?);", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            @NotNull final StrBuffer idListBuffer = new StrBuffer();
            collection
                    .stream()
                    .map(AbstractModel::getId)
                    .forEach(record -> idListBuffer.append("'").append(record).append("',"));
            @NotNull final String idList = idListBuffer.toString().substring(1, idListBuffer.length() - 2);
            statement.setString(1, idList);
            statement.executeUpdate();
        }
    }

    @NotNull
    @Override
    public M removeById(@NotNull final String id) throws AbstractException, SQLException {
        final M record = findOneById(id);
        return remove(record);
    }

    @NotNull
    public Collection<M> set(@NotNull final Collection<M> records) throws SQLException {
        clear();
        return add(records);
    }

    @Override
    public int totalCount() throws SQLException {
        @NotNull final String sql = String.format("" +
                "SELECT COUNT(*) as cnt " +
                "FROM %s;", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet rowSet = statement.executeQuery(sql);
            return rowSet.getInt("cnt");
        }
    }

}
