package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.User;

import java.security.GeneralSecurityException;

public interface IUserService extends IAbstractService<User> {

    @NotNull
    User checkUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException, GeneralSecurityException;

    @NotNull
    User create(
            @Nullable final String login,
            @Nullable final String password
    ) throws GeneralSecurityException, AbstractException;

    @NotNull
    User findByEmail(@Nullable final String email) throws AbstractException;

    @NotNull
    User findByLogin(@Nullable final String login) throws AbstractException;

    boolean isEmailExists(@Nullable final String email);

    boolean isLoginExists(@Nullable final String login);

    void lockUserByLogin(@Nullable final String login) throws AbstractException;

    @NotNull
    User removeByLogin(@Nullable final String login) throws AbstractException;

    @NotNull
    User setParameter(
            @Nullable final User user,
            @Nullable final EntityField paramName,
            @Nullable final String paramValue
    ) throws AbstractException;

    @NotNull
    User setPassword(
            @Nullable final String userId,
            @Nullable final String newPassword,
            @Nullable final String oldPassword
    ) throws AbstractException, GeneralSecurityException;

    @NotNull
    User setRole(
            @Nullable final String login,
            @Nullable final Role role
    ) throws AbstractException;

    void unlockUserByLogin(@Nullable final String login) throws AbstractException;

    @NotNull
    User updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws AbstractException;

}
