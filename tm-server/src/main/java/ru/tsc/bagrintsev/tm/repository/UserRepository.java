package ru.tsc.bagrintsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.repository.IUserRepository;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.IncorrectRoleException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectParameterNameException;
import ru.tsc.bagrintsev.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public User add(@NotNull final User user) throws SQLException {
        @NotNull final String sql = String.format("" +
                        "INSERT INTO %s (" +
                        "   id, login, password_hash, password_salt, email, first_name, middle_name, last_name, role, is_locked) " +
                        "VALUES (" +
                        "   ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setBytes(4, user.getPasswordSalt());
            statement.setString(5, user.getEmail());
            statement.setString(6, user.getFirstName());
            statement.setString(7, user.getMiddleName());
            statement.setString(8, user.getLastName());
            statement.setString(9, user.getRole().toString());
            statement.setBoolean(10, user.getLocked());
            statement.executeUpdate();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            final byte @NotNull [] salt
    ) throws SQLException, AbstractException {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setPasswordSalt(salt);
        return add(user);
    }

    @NotNull
    @Override
    protected User fetch(@NotNull final ResultSet rowSet) throws SQLException, IncorrectRoleException {
        @NotNull final User user = new User();
        user.setId(rowSet.getString("id"));
        user.setLogin(rowSet.getString("login"));
        user.setPasswordHash(rowSet.getString("password_hash"));
        user.setPasswordSalt(rowSet.getBytes("password_salt"));
        user.setEmail(rowSet.getString("email"));
        user.setFirstName(rowSet.getString("first_name"));
        user.setMiddleName(rowSet.getString("middle_name"));
        user.setLastName(rowSet.getString("last_name"));
        user.setRole(Role.toRole(rowSet.getString("role")));
        user.setLocked(rowSet.getBoolean("is_locked"));
        return user;
    }

    @NotNull
    @Override
    public User findByEmail(@NotNull final String email) throws UserNotFoundException, SQLException, IncorrectRoleException {
        @NotNull final String sql = String.format("" +
                "SELECT * " +
                "FROM %s " +
                "WHERE email = ?;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) throw new UserNotFoundException("Email", email);
            return fetch(rowSet);
        }
    }

    @NotNull
    @Override
    public User findByLogin(@NotNull final String login) throws UserNotFoundException, SQLException, IncorrectRoleException {
        @NotNull final String sql = String.format("" +
                "SELECT * " +
                "FROM %s " +
                "WHERE login = ?;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) throw new UserNotFoundException("Login", login);
            return fetch(rowSet);
        }
    }

    @NotNull
    @Override
    public User findOneById(@NotNull final String id) throws AbstractException, SQLException {
        @NotNull final String sql = String.format("" +
                "SELECT * " +
                "FROM %s " +
                "WHERE id = ?;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) throw new UserNotFoundException("Id", id);
            return fetch(rowSet);
        }
    }

    @Override
    protected @NotNull String getTableName() {
        return "m_user";
    }

    @NotNull
    @Override
    public User removeByLogin(@NotNull final String login) throws UserNotFoundException, SQLException, IncorrectRoleException {
        @NotNull final User user = findByLogin(login);
        @NotNull final String sql = String.format("" +
                "DELETE * " +
                "FROM %s " +
                "WHERE login = ?;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            statement.executeUpdate();
        }
        return user;
    }

    @NotNull
    @Override
    public User setParameter(
            @NotNull final User user,
            @NotNull final EntityField paramName,
            @NotNull final String paramValue
    ) throws AbstractException, SQLException {
        @NotNull final String sql = String.format("" +
                "UPDATE %s " +
                "SET    %s = ? " +
                "WHERE id = ?;", getTableName(), paramName.getDisplayName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            switch (paramName) {
                case EMAIL:
                case FIRST_NAME:
                case MIDDLE_NAME:
                case LAST_NAME:
                    statement.setString(1, paramValue);
                    break;
                case LOCKED:
                    statement.setBoolean(1, paramValue.equals("true"));
                    break;
                default:
                    throw new IncorrectParameterNameException(paramName, "User");
            }
            statement.setString(2, user.getId());
            statement.executeUpdate();
        }
        return findOneById(user.getId());
    }

    @NotNull
    @Override
    public User setRole(
            @NotNull final User user,
            @NotNull final Role role
    ) throws SQLException, AbstractException {
        @NotNull final String sql = String.format("" +
                "UPDATE %s " +
                "SET    role = ? " +
                "WHERE id = ?;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, role.toString());
            statement.setString(2, user.getId());
            statement.executeUpdate();
        }
        return findOneById(user.getId());
    }

    @Override
    public User setUserPassword(
            @NotNull final User user,
            @NotNull final String password,
            final byte @NotNull [] salt
    ) throws SQLException, AbstractException {
        @NotNull final String sql = String.format("" +
                "UPDATE %s " +
                "SET    password_hash = ? , " +
                "       password_salt = ? " +
                "WHERE id = ?;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, password);
            statement.setBytes(2, salt);
            statement.setString(3, user.getId());
            statement.executeUpdate();
        }
        return findOneById(user.getId());
    }

}
