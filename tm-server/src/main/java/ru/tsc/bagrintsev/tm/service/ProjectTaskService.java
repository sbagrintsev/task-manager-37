package ru.tsc.bagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectService;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectTaskService;
import ru.tsc.bagrintsev.tm.api.sevice.ITaskService;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.sql.SQLException;
import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    public ProjectTaskService(
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException, SQLException {
        check(EntityField.USER_ID, userId);
        checkIfEntityOK(projectId, taskId);
        return taskService.setProjectId(userId, taskId, projectId);
    }

    private void checkIfEntityOK(
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException, SQLException {
        check(EntityField.PROJECT_ID, projectId);
        check(EntityField.TASK_ID, taskId);
        if (!projectService.existsById(projectId)) throw new ProjectNotFoundException();
        if (!taskService.existsById(taskId)) throw new TaskNotFoundException();
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException, SQLException {
        check(EntityField.USER_ID, userId);
        check(EntityField.PROJECT_ID, projectId);
        @NotNull final List<Task> tasks = taskService.findAllByProjectId(userId, projectId);
        taskService.removeAll(tasks);
        projectService.removeById(userId, projectId);
    }

    @Override
    public Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId
    ) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.TASK_ID, taskId);
        return taskService.setProjectId(userId, taskId, null);
    }

}
